using Amazon.Lambda.Core;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace SimpleLambda1
{
    public class Function
    {

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool FunctionHandler(ILambdaContext context)
        {
            var isSuccess = false;

            context.Logger.Log("Begin Function");

            var invoices = new List<string>();

            try
            {
                using (var Conn = new SqlConnection(System.Environment.GetEnvironmentVariable("championProductsDBConnectionString")))
                {
                    using (var Cmd = new SqlCommand($"SELECT invoiceId from invoice", Conn))
                    {
                        Conn.Open();
                        SqlDataReader rdr = Cmd.ExecuteReader();
                        while (rdr.Read())
                        {
                            invoices.Add(rdr[0].ToString());
                        }
                    }
                }

                var Conn2 = new SqlConnection(System.Environment.GetEnvironmentVariable("championProductsDBConnectionString"));

                foreach (var invoice in invoices)
                {
                    using (var updateCmd = new SqlCommand($"UPDATE dbo.Invoice SET PaidDate = GETDATE() WHERE invoiceId = " + invoice, Conn2))
                    {
                        Conn2.Open();
                        updateCmd.ExecuteNonQuery();
                        Conn2.Close();
                    }
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }


            return isSuccess;
        }
    }
}